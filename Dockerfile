# syntax=docker/dockerfile:1

FROM node:12-alpine
RUN apk add --no-cache python2 g++ make
WORKDIR /mastering-docker
COPY . .
RUN yarn i --production
CMD ["node", "src/index.js]
EXPOSE 3000
